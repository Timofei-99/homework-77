const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const {nanoid} = require('nanoid');
const fileDb = require('../fileDb');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename(req, file, cb) {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();


router.get('/', (req, res) => {
    const messages = fileDb.getItems();
    res.send(messages)
});

router.post('/', upload.single('image'), (req, res) => {
    const message = req.body;
    if (message.message !== "") {
        if (message.author === "") {
            message.author = "Anonymus";
        }

        if (req.file) {
            message.image = req.file.filename;
        }

        fileDb.addItem(message);
        res.send(message);
    }
});


module.exports = router;
