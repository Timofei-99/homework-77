const express = require('express');
const cors = require('cors');
const messages = require('./app/messages');
const fileDb = require('./fileDb');


const app = express();

app.use(express.json());
app.use(express.static('publick'));
app.use(cors());

const port = 8000;

app.use('/messages', messages);
fileDb.init();
app.listen(port, () => {
    console.log('We are on ' + port + 'port');
});