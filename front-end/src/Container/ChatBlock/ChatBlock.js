import {makeStyles} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import Article from "../../Components/Artickle/Artickle";
import {getMessages} from "../../store/actions";

const useStyles = makeStyles((theme) => ({
    chat: {
        marginTop: "50px",
        marginBottom: "50px",
    },
}));

const ChatBlock = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const messages = useSelector(state => state.messages);


    useEffect(() => {
        dispatch(getMessages());
    }, [dispatch]);


    return (
        <Grid container spacing={3} direction="column" className={classes.chat} wrap="nowrap">
            {messages && messages.map(message => (
                <Article
                    key={message.id}
                    author={message.author}
                    message={message.message}
                    image={message.image}
                />
            ))}
        </Grid>
    );
};

export default ChatBlock;
