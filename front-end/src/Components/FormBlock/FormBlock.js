import React, {useState} from "react";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import {useDispatch} from "react-redux";
import {postMessages} from "../../store/actions";
import {Typography} from "@material-ui/core";

const FormBlock = () => {
    const dispatch = useDispatch();

    const [user, setUser] = useState({
        author: "",
        message: "",
        image: "",
    });


    const inputChangeHandler = (e) => {
        const name = e.target.name;
        const value = e.target.value;

        setUser((PrevState) => ({
            ...PrevState,
            [name]: value,
        }));
    };

    const fileChangeHandler = (e) => {
        const name = e.target.name;
        const file = e.target.files[0];

        setUser((prevState) => ({
            ...prevState,
            [name]: file,
        }));
    };

    const submitForm = (e) => {
        e.preventDefault();

        const formData = new FormData();
        Object.keys(user).forEach(key => {
            formData.append(key, user[key]);
        });
        dispatch(postMessages(formData));
    };

    return (
        <form onSubmit={submitForm}>
            <Typography variant="h4">New Post</Typography>
            <Grid container direction="column" spacing={2}>
                <Grid item xs>
                    <TextField
                        fullWidth
                        variant="outlined"
                        label="Author"
                        name="author"
                        value={user.author}
                        onChange={inputChangeHandler}
                    />
                </Grid>
                <Grid item xs>
                    <TextField
                        multiline
                        fullWidth
                        rows={5}
                        variant="outlined"
                        label="Message"
                        name="message"
                        value={user.message}
                        onChange={inputChangeHandler}
                        required
                    />
                </Grid>
                <Grid item xs>
                    <TextField
                        type="file"
                        name="image"
                        onChange={fileChangeHandler}
                    />
                </Grid>
                <Grid item xs>
                    <Button type="submit" color="primary" variant="contained" style={{margin: "10px"}}>
                        ADD
                    </Button>
                </Grid>
            </Grid>
        </form>
    );
};

export default FormBlock;
