import axios from "axios";
import {apiURL} from "./config";

const axiosAPI = axios.create({
    baseURL: apiURL,
});

export default axiosAPI;