import axiosAPI from "../axiosAPI";

export const GET_MESSAGES_REQUEST = 'GET_MESSAGES_REQUEST';
export const GET_MESSAGES_SUCCESS = 'GET_MESSAGES_SUCCESS';
export const GET_MESSAGES_FAILURE = 'GET_MESSAGES_FAILURE';

export const POST_MESSAGES_REQUEST = 'POST_MESSAGES_REQUEST';
export const POST_MESSAGES_SUCCESS = 'POST_MESSAGES_SUCCESS';
export const POST_MESSAGES_FAILURE = 'POST_MESSAGES_FAILURE';


export const getMessagesRequest = () => ({type: GET_MESSAGES_REQUEST});
export const getMessagesSuccess = (messages) => ({type: GET_MESSAGES_SUCCESS, payload: messages});
export const getMessagesFailure = () => ({type: GET_MESSAGES_FAILURE});

export const postMessagesRequest = () => ({type: POST_MESSAGES_REQUEST});
export const postMessagesSuccess = () => ({type: POST_MESSAGES_SUCCESS});
export const postMessagesFailure = () => ({type: POST_MESSAGES_FAILURE});

export const getMessages = () => {
    return async (dispatch) => {
        try {
            dispatch(getMessagesRequest());
            const {data} = await axiosAPI.get('/messages');
            dispatch(getMessagesSuccess(data));
        } catch (e) {
            dispatch(getMessagesFailure(e));
        }
    }
};

export const postMessages = (messages) => {
    return async (dispatch) => {
        try {
            dispatch(postMessagesRequest());
            await axiosAPI.post('/messages', messages);
            dispatch(postMessagesSuccess());
        } catch (e) {

        }
    }
};