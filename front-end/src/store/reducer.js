import {
    GET_MESSAGES_FAILURE,
    GET_MESSAGES_REQUEST,
    GET_MESSAGES_SUCCESS, POST_MESSAGES_FAILURE,
    POST_MESSAGES_REQUEST,
    POST_MESSAGES_SUCCESS
} from "./actions";

const initialState = {
    messages: [],
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_MESSAGES_REQUEST:
            return {...state}
        case GET_MESSAGES_SUCCESS:
            return {...state, messages: action.payload}
        case GET_MESSAGES_FAILURE:
            return {...state}
        case POST_MESSAGES_REQUEST:
            return {...state}
        case POST_MESSAGES_SUCCESS:
            return {...state}
        case POST_MESSAGES_FAILURE:
            return {...state}
        default:
            return state;
    }
}

export default reducer;