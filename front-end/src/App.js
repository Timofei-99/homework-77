import './App.css';
import {Container} from "@material-ui/core";
import FormBlock from "./Components/FormBlock/FormBlock";
import ChatBlock from "./Container/ChatBlock/ChatBlock";

function App() {
    return (
        <div className="App">
            <Container>
                <FormBlock/>
            </Container>
            <Container>
                <ChatBlock/>
            </Container>
        </div>
    );
}

export default App;
